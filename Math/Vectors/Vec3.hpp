#pragma once

class Vec3
{
public:
  Vec3() = default;
  Vec3(float X, float Y, float Z);
  Vec3(const Vec3& Copy);
  Vec3(Vec3&& Move);
  Vec3& operator=(const Vec3& Rhs);
  Vec3& operator=(Vec3&& Rhs);
  Vec3& operator+=(const Vec3& Rhs);
  Vec3& operator+=(Vec3&& Rhs);
  Vec3& operator-=(const Vec3& Rhs);
  Vec3& operator-=(Vec3&& Rhs);
  Vec3 operator+(const Vec3& Rhs) const;
  Vec3 operator+(Vec3&& Rhs) const;
  Vec3 operator-(const Vec3& Rhs) const;
  Vec3 operator-(Vec3&& Rhs) const;
  inline float GetX() const { return X; }
  inline float GetY() const { return Y; }
  inline float GetZ() const { return Z; }
  inline void SetX(float XX) { X = XX; }
  inline void SetY(float YY) { Y = YY; }
  inline void SetZ(float ZZ) { Z = ZZ; }
  float GetNorm() const;
  float GetNormSquared() const;
  
  
  float Dot(const Vec3& Other) const;
private:
  float X{0}, Y{0}, Z{0};
};
