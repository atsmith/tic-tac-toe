#include "Vec3.hpp"
#include <iostream>
using namespace std;

int main(int argc, char** argv)
{
  Vec3 Vector{};
  Vec3 Vector2{1, 2, 3};
  Vec3 Vector3{3, 2, 1};
  Vector2 += Vector3;
  cout << "Vector: <" << Vector.GetX() << ", " << Vector.GetY() << ", " << Vector.GetZ() << ">" << endl;
  cout << "Vector2: <" << Vector2.GetX() << ", " << Vector2.GetY() << ", " << Vector2.GetZ() << ">" << endl;
  cout << "Vector norm: " << Vector2.GetNorm() << endl;
  cout << "Vector norm squared: " << Vector2.GetNormSquared() << endl;
  return 0;
}
