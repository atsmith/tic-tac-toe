@SETLOCAL
@ECHO OFF

SET FILES=../Vec3.cpp ../main.cpp
SET FLAGS=/W3 /EHsc /Od /RTC1 /FeVectors.exe /I../

if not exist bin mkdir bin

pushd bin

call ../vcenv.bat x64 %FLAGS% %FILES%

popd bin

@ENDLOCAL
