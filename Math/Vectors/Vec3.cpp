#include "Vec3.hpp"
#include <algorithm>
using namespace std;

Vec3::Vec3(float X, float Y, float Z)
  : X{X}, Y{Y}, Z{Z}
{}

Vec3::Vec3(const Vec3& Copy)
  : X{Copy.X}, Y{Copy.Y}, Z{Copy.Z}
{}

Vec3::Vec3(Vec3&& Move)
  : X{move(Move.X)}, Y{move(Move.Y)}, Z{move(Move.Z)}
{}

Vec3& Vec3::operator=(const Vec3& Rhs)
{
  X = Rhs.X;
  Y = Rhs.Y;
  Z = Rhs.Z;
  return *this;
}

Vec3& Vec3::operator=(Vec3&& Rhs)
{
  X = move(Rhs.X);
  Y = move(Rhs.Y);
  Z = move(Rhs.Z);
  return *this;
}

Vec3& Vec3::operator+=(const Vec3& Rhs)
{
  X += Rhs.GetX();
  Y += Rhs.GetY();
  Z += Rhs.GetZ();
  return *this;
}

Vec3& Vec3::operator+=(Vec3&& Rhs)
{
  X += move(Rhs.X);
  Y += move(Rhs.Y);
  Z += move(Rhs.Z);
  return *this;
}

Vec3& Vec3::operator-=(const Vec3& Rhs)
{
  X -= Rhs.X;
  Y -= Rhs.Y;
  Z -= Rhs.Z;
  return *this;
}

Vec3& Vec3::operator-=(Vec3&& Rhs)
{
  X -= move(Rhs.X);
  Y -= move(Rhs.Y);
  Z -= move(Rhs.Z);
  return *this;
}

Vec3 Vec3::operator+(const Vec3& Rhs) const
{
  return Vec3{X + Rhs.GetX(), Y + Rhs.GetY(), Z + Rhs.GetZ()};
}

Vec3 Vec3::operator+(Vec3&& Rhs) const
{
  return Vec3{X + move(Rhs.GetX()), Y + move(Rhs.GetY()), Z + move(Rhs.GetZ())};
}

Vec3 Vec3::operator-(const Vec3& Rhs) const
{
  return Vec3{X - Rhs.GetX(), Y - Rhs.GetY(), Z - Rhs.GetZ()};
}

Vec3 Vec3::operator-(Vec3&& Rhs) const
{
  return Vec3{X - move(Rhs.GetX()), Y - move(Rhs.GetY()), Z - move(Rhs.GetZ())};
}

float Vec3::GetNorm() const
{
  return sqrt(Dot(*this));
}

float Vec3::GetNormSquared() const
{
  return Dot(*this);
}

float Vec3::Dot(const Vec3& Other) const
{
  return
    X * Other.GetX() +
    Y * Other.GetY() +
    Z * Other.GetZ();
}
