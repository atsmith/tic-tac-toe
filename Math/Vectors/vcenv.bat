@SETLOCAL
@ECHO off

set NewLine=echo.

set VCVARSALL="%VS120COMNTOOLS%"
if %VCVARSALL%=="" (
   echo Did not find vcvarsall.bat.  Please install Visual Studio 2013 Community Edition or greater.
   echo Exiting... & %NewLine% & %NewLine%
   goto end
)
set VCVARSALL=%VCVARSALL:Common7\Tools\=VC\vcvarsall.bat%
if not exist %VCVARSALL% (
  echo Did not find vcvarsall.bat.  Please install Visual Studio 2013 Community Edition or greater.
  echo Exiting... & %NewLine%
  goto end
) else (
  
  echo Found vcvarsall.bat. & %NewLine%
)

call %VCVARSALL% %1

for /F "tokens=1,* delims= " %%a in ("%*") do set ARGS=%%b

cl %ARGS%

@ENDLOCAL
