#include "Win32GLRenderer.hpp"
#include "Mathf.hpp"
#include <GL/GL.h>

Win32GLRenderer::Win32GLRenderer() :
  DeviceContext{ 0 },
  RenderContext{ 0 }
{
  
}

Win32GLRenderer::Win32GLRenderer(const Win32GLRenderer&) :
  DeviceContext{ 0 },
  RenderContext{ 0 }
{
  
}

Win32GLRenderer::~Win32GLRenderer()
{
  Destroy();
}

Win32GLRenderer& Win32GLRenderer::operator=(const Win32GLRenderer&)
{
  return *this;
}

bool Win32GLRenderer::Create(Win32Window& window)
{
  if(NULL == window.GetWindow())
  {
    return false;
  }

  WindowHandle = window.GetWindow();
  DeviceContext = GetDC(WindowHandle);
  PIXELFORMATDESCRIPTOR pfd;
  int PixelFormat{ 0 };
  if(NULL == DeviceContext)
  {
    return false;
  }

  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
  pfd.iPixelType = PFD_TYPE_RGBA;
  pfd.cColorBits = 24;
  pfd.cDepthBits = 24;
  pfd.cStencilBits = 8;

  PixelFormat = ChoosePixelFormat(DeviceContext, &pfd);
  if(FALSE == PixelFormat)
  {
    Destroy();
    return false;
  }

  if(FALSE == SetPixelFormat(DeviceContext, PixelFormat, &pfd))
  {
    Destroy();
    return false;
  }

  RenderContext = wglCreateContext(DeviceContext);
  if(FALSE == RenderContext)
  {
    Destroy();
    return false;
  }

  if(FALSE == wglMakeCurrent(DeviceContext,RenderContext))
  {
    Destroy();
    return false;
  }

  return true;
}

int Win32GLRenderer::Destroy()
{
  int error{ 0 };
  error = wglMakeCurrent(NULL, NULL);
  if(FALSE != RenderContext)
  {
    if(FALSE == wglDeleteContext(RenderContext) && FALSE == error)
    {
      error = GetLastError();
    }
  }
  if(FALSE != DeviceContext)
  {
    if(FALSE == ReleaseDC(WindowHandle, DeviceContext) && FALSE == error)
    {
      error = GetLastError();
    }
  }
  RenderContext = NULL;
  DeviceContext = NULL;
  return error;
}

void Win32GLRenderer::Display()
{
  SwapBuffers(DeviceContext);
}

void Win32GLRenderer::SetClearColor(float red, float green, float blue, float alpha)
{
  if(RenderContext)
  {
    glClearColor(mathf::Clamp01(red), mathf::Clamp01(green), mathf::Clamp01(blue), mathf::Clamp01(alpha));
  }
}

void Win32GLRenderer::ClearScreen()
{
  if(RenderContext)
  {
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  }
}
