#include "Win32GLInit.hpp"
#include "Win32Window.hpp"
#include "Win32GLRenderer.hpp"
#include <GL/GL.h>

PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB{ NULL };
PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB{ NULL };

static void DestroyTemporaryContext(Win32Window& window, Win32GLRenderer& renderer)
{
  renderer.Destroy();
  window.Destroy();
}

bool GLInit()
{
  static bool Initialized{false};
  if(Initialized)
  {
    return true;
  }

  Win32Window Window{ GetModuleHandle( 0 ) };
  Win32GLRenderer Renderer{ };

  if(!Window.Create("Temp", 1, 1))
  {
    return false;
  }

  if(!Renderer.Create(Window))
  {
    Window.Destroy();
    return false;
  }

  wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)wglGetProcAddress("wglChoosePixelFormatARB");
  if(NULL == wglChoosePixelFormatARB)
  {
    DestroyTemporaryContext(Window, Renderer);
    return false;
  }

  wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");
  if(NULL == wglCreateContextAttribsARB)
  {
    DestroyTemporaryContext(Window, Renderer);
    return false;
  }

  DestroyTemporaryContext(Window, Renderer);
  Initialized = true;
  return true;
}
