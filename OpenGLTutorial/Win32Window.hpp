/* Win32Window.hpp

   Copyright (C) 2015 Anthony Smith

   For conditions of distribution and use, see copyright notice in main.cpp

*/
#ifndef WIN32_WINDOW_HPP
#define WIN32_WINDOW_HPP 1

#include <Windows.h>

class Win32Window
{
public:
  Win32Window(HINSTANCE);
  ~Win32Window();
  bool Create(const char*,
	      int,
	      int);
  void Show(int cmdShow = SW_SHOWDEFAULT);
  int Destroy();
  bool PollEvents() const;
  HWND GetWindow() const;
private:
  HINSTANCE Instance;
  HWND WindowHandle;
}; // end Win32Window class

#endif
