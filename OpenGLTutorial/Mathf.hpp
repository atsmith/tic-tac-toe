#ifndef MATHF_HPP
#define MATHF_HPP 1

namespace mathf
{
  template<typename T>
  T Clamp(T val, T min, T max)
  {
    if(val < min)
    {
      return min;
    }

    if(val > max)
    {
      return max;
    }

    return val;
  }

  template<typename T>
  T Clamp01(T val)
  {
    return Clamp(val, (T)0, (T)1);
  }
} // end mathf namespace

#endif
