#ifndef WIN32_OPENGL_INIT_HPP
#define WIN32_OPENGL_INIT_HPP 1

#include <Windows.h>

//wglChoosePixelFormatARB
#define WGL_DRAW_TO_WINDOW_ARB    0x2001
#define WGL_ACCELERATION_ARB      0x2003
#define WGL_SWAP_METHOD_ARB       0x2007
#define WGL_SUPPORT_OPENGL_ARB    0x2010
#define WGL_DOUBLE_BUFFER_ARB     0x2011
#define WGL_PIXEL_TYPE_ARB        0x2013
#define WGL_COLOR_BITS_ARB        0x2014
#define WGL_DEPTH_BITS_ARB        0x2022
#define WGL_STENCIL_BITS_ARB      0x2023
//wglChoosePixelFormatARB accepted values
#define WGL_FULL_ACCELERATION_ARB 0x2027
#define WGL_SWAP_EXCHANGE_ARB     0x2028
#define WGL_TYPE_RGBA_ARB         0x202B

//wglCreateContextAttribsARB
#define WGL_CONTEXT_MAJOR_VERSION_ARB 0x2091
#define WGL_CONTEXT_MINOR_VERSION_ARB 0x2092

typedef BOOL (WINAPI* PFNWGLCHOOSEPIXELFORMATARBPROC)(HDC,
						      const int*,
						      const FLOAT*,
						      UINT,
						      int*,
						      UINT*);
typedef HGLRC (WINAPI* PFNWGLCREATECONTEXTATTRIBSARBPROC)(HDC,
							  HGLRC,
							  const int*);

extern PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB;
extern PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB;

bool GLInit();

#endif
