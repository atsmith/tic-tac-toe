#ifndef WIN32_OPENGL_RENDERER
#define WIN32_OPENGL_RENDERER 1

#include "Win32Window.hpp"

class Win32GLRenderer
{
public:
  Win32GLRenderer();
  ~Win32GLRenderer();
  bool Create(Win32Window&);
  int Destroy();
  void Display();
  void SetClearColor(float, float, float, float);
  void ClearScreen();
private:
  Win32GLRenderer(const Win32GLRenderer&);
  Win32GLRenderer& operator=(const Win32GLRenderer&);
  HWND WindowHandle;
  HDC DeviceContext;
  HGLRC RenderContext;
}; // end Win32GLRenderer class

#endif
