#include "Win32GL4Renderer.hpp"
#include "Win32GLInit.hpp"
#include "Mathf.hpp"
#include <GL/GL.h>

Win32GL4Renderer::Win32GL4Renderer() :
  DeviceContext{ 0 },
  RenderContext{ 0 }
{

}

Win32GL4Renderer::Win32GL4Renderer(const Win32GL4Renderer&) :
  DeviceContext{ 0 },
  RenderContext{ 0 }
{
    
}

Win32GL4Renderer::~Win32GL4Renderer()
{
  Destroy();
}

Win32GL4Renderer& Win32GL4Renderer::operator=(const Win32GL4Renderer&)
{
  return *this;
}

bool Win32GL4Renderer::Create(Win32Window& window)
{
  if(!GLInit())
  {
    return false;
  }
  
  if(NULL == window.GetWindow())
  {
    return false;
  }

  WindowHandle = window.GetWindow();

  DeviceContext = GetDC(WindowHandle);
  if(FALSE == DeviceContext)
  {
    return false;
  }

  int ContextAttribs[5];
  int PixelAttribs[19];
  int PixelFormat;
  UINT PixelFormatCount;
  PIXELFORMATDESCRIPTOR pfd;

  PixelAttribs[0] = WGL_SUPPORT_OPENGL_ARB;
  PixelAttribs[1] = TRUE;
  PixelAttribs[2] = WGL_DRAW_TO_WINDOW_ARB;
  PixelAttribs[3] = TRUE;
  PixelAttribs[4] = WGL_ACCELERATION_ARB;
  PixelAttribs[5] = WGL_FULL_ACCELERATION_ARB;
  PixelAttribs[6] = WGL_COLOR_BITS_ARB;
  PixelAttribs[7] = 24;
  PixelAttribs[8] = WGL_DEPTH_BITS_ARB;
  PixelAttribs[9] = 24;
  PixelAttribs[10] = WGL_DOUBLE_BUFFER_ARB;
  PixelAttribs[11] = TRUE;
  PixelAttribs[12] = WGL_SWAP_METHOD_ARB;
  PixelAttribs[13] = WGL_SWAP_EXCHANGE_ARB;
  PixelAttribs[14] = WGL_PIXEL_TYPE_ARB;
  PixelAttribs[15] = WGL_TYPE_RGBA_ARB;
  PixelAttribs[16] = WGL_STENCIL_BITS_ARB;
  PixelAttribs[17] = 8;
  PixelAttribs[18] = NULL;

  if(FALSE == wglChoosePixelFormatARB(DeviceContext, PixelAttribs, NULL, 1, &PixelFormat, &PixelFormatCount))
  {
    Destroy();
    return false;
  }

  if(FALSE == SetPixelFormat(DeviceContext, PixelFormat, &pfd))
  {
    Destroy();
    return false;
  }

  ContextAttribs[0] = WGL_CONTEXT_MAJOR_VERSION_ARB;
  ContextAttribs[1] = 4;
  ContextAttribs[2] = WGL_CONTEXT_MINOR_VERSION_ARB;
  ContextAttribs[3] = 0;
  ContextAttribs[4] = NULL;

  RenderContext = wglCreateContextAttribsARB(DeviceContext, NULL, ContextAttribs);
  if(FALSE == RenderContext)
  {
    Destroy();
    return false;
  }

  if(FALSE == wglMakeCurrent(DeviceContext, RenderContext))
  {
    Destroy();
    return false;
  }

  return true;
}

int Win32GL4Renderer::Destroy()
{
  int error{ 0 };
  if(FALSE == wglMakeCurrent(NULL, NULL))
  {
    error = GetLastError();
  }

  if(NULL != RenderContext)
  {
    if(FALSE == wglDeleteContext( RenderContext) && FALSE == error)
    {
      error = GetLastError();
    }
  }

  if(NULL != DeviceContext)
  {
    if(FALSE == ReleaseDC(WindowHandle, DeviceContext) && FALSE == error)
    {
      error = GetLastError();
    }
  }

  RenderContext = NULL;
  DeviceContext = NULL;
  
  return error;
}

void Win32GL4Renderer::Display()
{
  if(NULL != DeviceContext)
  {
    SwapBuffers(DeviceContext);
  }
}

void Win32GL4Renderer::SetClearColor(float red, float green, float blue, float alpha)
{
  if(RenderContext)
  {
    glClearColor(mathf::Clamp01(red), mathf::Clamp01(green), mathf::Clamp01(blue), mathf::Clamp01(alpha));
  }
}

void Win32GL4Renderer::ClearScreen()
{
  if(NULL != RenderContext)
  {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  }
}
