#ifndef WIN32_GL4_RENDERER_HPP
#define WIN32_GL4_RENDERER_HPP 1

#include "Win32Window.hpp"

class Win32GL4Renderer
{
public:
  Win32GL4Renderer();
  ~Win32GL4Renderer();
  bool Create(Win32Window&);
  int Destroy();
  void Display();
  void SetClearColor(float, float, float, float);
  void ClearScreen();
private:
  Win32GL4Renderer(const Win32GL4Renderer&);
  Win32GL4Renderer& operator=(const Win32GL4Renderer&);
  HWND WindowHandle;
  HDC DeviceContext;
  HGLRC RenderContext;
}; // end Win32GL4Renderer class

#endif
