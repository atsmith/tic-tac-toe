/* Win32Window.cpp

   Copyright (C) 2015 Anthony Smith

   For conditions of distribution and use, see copyright notice in main.cpp

*/
#include "Win32Window.hpp"

static const char* ClassName{ "WindowTutorial" };
static WNDCLASSEX wclass;
static unsigned int WindowClassRefCount{ 0 };

static LRESULT CALLBACK WinProc(HWND hWnd,
				UINT msg,
				WPARAM wParam,
				LPARAM lParam)
{
  switch(msg)
  {
  case WM_CLOSE:
    PostQuitMessage(0);
    return 0;
  }

  return DefWindowProc(hWnd, msg, wParam, lParam);
}

static bool Register(HINSTANCE hInstance)
{
  if(0 == WindowClassRefCount)
  {
    wclass.cbSize = sizeof(WNDCLASSEX);
    wclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wclass.lpfnWndProc = WinProc;
    wclass.cbClsExtra = 0;
    wclass.cbWndExtra = 0;
    wclass.hInstance = hInstance;
    wclass.hIcon = (HICON)LoadImage(NULL,
				    IDI_APPLICATION,
				    IMAGE_ICON,
				    LR_DEFAULTSIZE,
				    LR_DEFAULTSIZE,
				    LR_SHARED);
    wclass.hCursor = (HCURSOR)LoadImage(NULL,
					IDC_ARROW,
					IMAGE_CURSOR,
					LR_DEFAULTSIZE,
					LR_DEFAULTSIZE,
					LR_SHARED);
    wclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    wclass.lpszMenuName = NULL;
    wclass.lpszClassName = ClassName;
    wclass.hIconSm = wclass.hIcon;

    bool registered = (FALSE != RegisterClassEx(&wclass));

    if(registered)
    {
      WindowClassRefCount++;
    }

    return registered;
    
  }
  else
  {
    WindowClassRefCount++;
  }

  return true;
}

static bool Unregister(HINSTANCE hInstance)
{
  if(1 < WindowClassRefCount)
  {
    WindowClassRefCount--;
    return true;
  }
  int unregistered = UnregisterClass(ClassName, hInstance);
  if(FALSE != unregistered)
  {
    WindowClassRefCount--;
    return true;
  }
  return false;
}

Win32Window::Win32Window(HINSTANCE instance) :
  WindowHandle{ 0 },
  Instance{ instance }
{

}

Win32Window::~Win32Window()
{
  Destroy();
}

bool Win32Window::Create(const char* title,
			 int width,
			 int height)
{
  if(!Register(Instance))
  {
    return false;
  }

  WindowHandle = CreateWindowEx(NULL,
				ClassName,
				title,
				WS_OVERLAPPEDWINDOW | WS_VISIBLE,
				0, 0,
				width,height,
				NULL,
				NULL,
				Instance,
				NULL);
  return FALSE != WindowHandle;
}

void Win32Window::Show(int cmdShow)
{
  ShowWindow(WindowHandle, cmdShow);
}

int Win32Window::Destroy()
{
  int error{ 0 };
  int destroyed = DestroyWindow(WindowHandle);
  WindowHandle = NULL;
  if(FALSE == destroyed)
  {
    error = GetLastError();
  }

  if(!Unregister(Instance) && FALSE == error )
  {
    error = GetLastError();
  }

  return error;
}

bool Win32Window::PollEvents() const
{
  MSG msg;
  while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
  {
    if(WM_QUIT == msg.message)
    {
      return false;
    }
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }
  return true;
}

HWND Win32Window::GetWindow() const
{
  return WindowHandle;
}
