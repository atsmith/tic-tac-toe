@echo off

set NewLine=echo.
%NewLine%
echo Checking for vcvarsall.bat...
set VCVARSALL="%VS120COMNTOOLS%"
if %VCVARSALL%=="" (
   echo Did not find vcvarsall.bat.  Please install Visual Studio 2013 Community Edition or greater.
   echo Exiting... & %NewLine% & %NewLine%
   goto end
)
set VCVARSALL=%VCVARSALL:Common7\Tools\=VC\vcvarsall.bat%
if not exist %VCVARSALL% (
  echo Did not find vcvarsall.bat.  Please install Visual Studio 2013 Community Edition or greater.
  echo Exiting... & %NewLine%
  goto end
) else (
  
  echo Found vcvarsall.bat. & %NewLine% & %NewLine%
)

if not exist Debug mkdir Debug
if not exist Debug\Obj mkdir Debug\Obj
pushd Debug
echo Cleaning... & echo.
@REM Remove all created object files from previous builds.
@REM This forces a recompilation of the C++ files that are
@REM specified somewhere below.
if exist Obj\*.obj del Obj\*.obj

@REM The name of the executable once compiled.
set OUTPUTNAME=Main.exe
@REM The warning level and which warnings to ignore.
@REM Warning 4100 is ignored because it will come up with every compile
@REM because hPrevInstance and lpCmdLine are currently not being used.
set WARNINGS=/W 4 /wd4100
@REM The options for the compiler.
set OPTS=%WARNINGS% /EHsc /Zi /MTd /Fe%OUTPUTNAME% /FoObj\
@REM The 3rd party libraries that we need to compile against.
set LIBS=User32.lib Gdi32.lib Opengl32.lib
@REM The files that we are going to be compiling and linking.
set FILES=../main.cpp ../Win32Window.cpp ../Win32GLRenderer.cpp ../Win32GL4Renderer.cpp ../Win32GLInit.cpp
@REM The options for the linker
set LINK=%LIBS%

@REM Have Visual Studio setup the defaults for the compiler
@REM and the linker so that we don't have to.
call %VCVARSALL% x86

@REM Compile and link now.
cl %OPTS% %FILES% %LINK% & %NewLine% & %NewLine%

if exist %OUTPUTNAME% (
   echo Build successful. & %NewLine%
) else (
   echo Build failed. & %NewLine%
)

popd
@echo on
