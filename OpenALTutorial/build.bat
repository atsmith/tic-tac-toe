@SETLOCAL
@echo off

if exist bin rmdir /S /Q bin
if exist lib rmdir /S /Q lib
if exist include rmdir /S /Q include
if exist AL rmdir /S /Q AL
if exist share rmdir /S /Q share
if exist Debug rmdir /S /Q Debug

set NewLine=echo.

echo Checking for vcvarsall.bat...
set VCVARSALL="%VS120COMNTOOLS%"
if %VCVARSALL%=="" (
   echo Did not find vcvarsall.bat.  Please install Visual Studio 2013 Community Edition or greater.
   echo Exiting... & %NewLine% & %NewLine%
   goto end
)
set VCVARSALL=%VCVARSALL:Common7\Tools\=VC\vcvarsall.bat%
if not exist %VCVARSALL% (
  echo Did not find vcvarsall.bat.  Please install Visual Studio 2013 Community Edition or greater.
  echo Exiting... & %NewLine%
  goto end
) else (
  
  echo Found vcvarsall.bat. & %NewLine%
)

echo Checking for devenv...

for /f "tokens=3*" %%x in ('REG QUERY "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\devenv.exe"') do set DEVENV="%%x %%y"

if %DEVENV% == "" (

echo Did not find devenv.  Please install Visual Studio 2013 Community Edition or greater.

echo Exiting... & %NewLine%

goto end

) else (

echo Found devenv. & %NewLine%

)


pushd openal-soft-1.16.0

if not exist Build/Win32 mkdir "Build/Win32"

pushd Build
pushd Win32
echo Generating OpenAL Soft project files for Visual Studio 2013 CE...
cmake -DDSOUND_INCLUDE_DIR:string="C:/Program Files (x86)/Microsoft SDKs/Windows/v7.1A/Include" -DDSOUND_LIBRARY:string="C:/Program Files (x86)/Microsoft SDKs/Windows/v7.1A/Lib/dsound.lib" -DCMAKE_BUILD_TYPE:string="Debug" -DCMAKE_CONFIGURATION_TYPES:string="Debug;Release" -DCMAKE_INSTALL_PREFIX:string="../../../" -DALSOFT_BACKEND_DSOUND:bool=on -DALSOFT_BACKEND_MMDEVAPI:bool=on -DALSOFT_BACKEND_WAVE:bool=on -DALSOFT_BACKEND_WINMM:bool=on -DALSOFT_CONFIG:bool=on -DALSOFT_CPUEXT_SSE:bool=on -DALSOFT_CPUEXT_SSE2:bool=on -DALSOFT_CPUEXT_SSE4_1:bool=on -DALSOFT_DLOPEN:bool=on -DALSOFT_EXAMPLES:bool=off -DALSOFT_HRTF_DEFS:bool=on -DALSOFT_REQUIRE_DSOUND:bool=on -DALSOFT_UTILS:bool=on -G "Visual Studio 12 2013" ../../ > NUL 2>&1
echo Done. & %NewLine%

echo Building OpenAL Soft debug dll...
%DEVENV% /build "Debug|Win32" /project "INSTALL" OpenAL.sln > NUL 2>&1
echo Done. & %NewLine%
@REM Get out of Win32
popd
@REM Get out of Build
popd

echo Cleaning up after OpenAL Soft build...
rmdir /S /Q Build
echo Done. & %NewLine%

@REM Get out of OpenAl Soft
popd

if not exist AL mkdir AL

pushd include
pushd AL
for %%f in (*.h, *.hpp) do move %%f "../../AL" > NUL 2>&1
@REM Get out of AL
popd
@REM Get out of include
popd

rmdir /S /Q include
rmdir /S /Q share

if not exist Debug mkdir Debug

pushd bin
del *.exe
move OpenAL32.dll "../Debug" > NUL 2>&1
@REM Get out of bin
popd

rmdir /S /Q bin

pushd lib
rmdir /S /Q pkgconfig
@REM Get out of lib
popd

echo Building Tutorial... & %NewLine%

pushd Debug
call %VCVARSALL% x86

set OUTNAME=OpenALTutorial.exe
set OPTS=/W4 /wd4100 /EHsc /RTC1 /Od /Zi /Fe%OUTNAME% /I..\ /DWIN32
set FILES=../main.cpp
set LIBS=../lib/OpenAL32.lib

cl %OPTS% %FILES% %LIBS%

echo Done. & %NewLine%

popd

:end
@echo on
